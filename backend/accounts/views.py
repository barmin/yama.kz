import json

from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponse
from django.contrib.auth import login, get_user_model
from accounts.forms import LoginForm, RegisterForm


def login_view(request):
    if request.method == 'POST':
        login_form = LoginForm(request, request.POST)
        response_data = {}
        if login_form.is_valid():
            user = login_form.get_user()
            response_data['success'] = True
            login(request, user)
        else:
            response_data['success'] = False
            response_data['error'] = 'Проверьте правильность введенных данных'

        return HttpResponse(json.dumps(response_data),
                            content_type="application/json")


def forgot_password(request):
    response_data = {}
    if request.method == 'POST':
        email = request.POST.get('email')
        user = get_user_model().objects.filter(email=email).first()
        if user:
            password: get_user_model() = get_user_model().objects.make_random_password()
            user.set_password(password)

            text = (
                'Вы забыли свой пароль от сайта yama.kz?\n\n'
                'Вот ваш новый пароль: \n\n'
                f'{password}\n\n'
                'Если смену пароля запрашивали не вы, то просто зайдите и смените ваш пароль на сайте\n\n'
                f'C уважением администрация сайта yama.kz'
            )
            send_mail(
                f'Восстановление забытого пароля на сайте yama.kz',
                text,
                settings.EMAIL_HOST_USER,
                [email], fail_silently=False)
            user.save()
            response_data['success'] = True
        else:
            response_data['success'] = False
            response_data['error'] = 'Пользователь не найден'
    return HttpResponse(json.dumps(response_data),
                        content_type="application/json")


def registration_view(request):
    if request.method == 'POST':
        register_form = RegisterForm(request.POST)
        response_data = {}
        if register_form.is_valid():
            if request.recaptcha_is_valid:
                user = register_form.save()
                response_data['success'] = True
                login(request, user)
            else:
                response_data['success'] = False
                response_data['error'] = 'Ошибка капчи'
        else:
            response_data['success'] = False
            response_data['error'] = 'Проверьте правильность введенных данных'

        return HttpResponse(json.dumps(response_data),
                            content_type="application/json")

# ass RegisterView(FormView):
#     form_class = RegisterForm
#
#     def form_valid(self, form):
#         # проверка валидности reCAPTCHA
#         if self.request.recaptcha_is_valid:
#             form.save()
#             return HttpResponse({'test': 'test'}, self.get_context_data())
#         return HttpResponse({'test': 'test'}, self.get_context_data())
