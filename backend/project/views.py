import json

from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import FormView
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

from accounts.forms import RegisterForm


def about(request) -> HttpResponse:
    return redirect('/')
    return render(request, 'about.html')

def faq(request) -> HttpResponse:
    return redirect('/')
    return render(request, 'faq.html')

def license(request) -> HttpResponse:
    return redirect('/')
    return render(request, 'license.html')


# class RegisterView(FormView):
#     form_class = RegisterForm
#
#     def form_valid(self, form):
#         # проверка валидности reCAPTCHA
#         if self.request.recaptcha_is_valid:
#             form.save()
#             return HttpResponse({'test': 'test'}, self.get_context_data())
#         return HttpResponse({'test': 'test'}, self.get_context_data())

