from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include, re_path
from django.views.generic import RedirectView

import yama.views as yama_views
from accounts.views import login_view, registration_view, forgot_password
from yama.decorators import check_recaptcha
from .views import about, faq, license

handler404 = 'yama.views.page404'


urlpatterns = [
    path('admin/', admin.site.urls),
    path('pits/', include('yama.urls')),
    path('about/', about, name="about"),
    path('faq/', faq, name="faq"),
    path('license/', license, name="user_license"),
    path('logout/', LogoutView.as_view(), name="logout"),
    # TODO: пофиксить на логин

    path('account/', include('django.contrib.auth.urls')),

    path('login/', login_view, name='login'),
    path('forgot_password/', forgot_password, name='forgot_password'),
    # path('register/', registration_view, name='register'),
    path('register/', check_recaptcha(registration_view), name='register'),
    path('', yama_views.index, name="index")
]

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # urlpatterns += [
    #     path('favicon.ico', RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico'))
    # ]
else:
    import django.views.static

    urlpatterns += [
        re_path(r'^static/(?P<path>.*)$', django.views.static.serve,
                {'document_root': settings.STATIC_ROOT, 'show_indexes': settings.DEBUG}),
        re_path(r'^media/(?P<path>.*)$', django.views.static.serve,
                {'document_root': settings.MEDIA_ROOT, 'show_indexes': settings.DEBUG}),
    ]
