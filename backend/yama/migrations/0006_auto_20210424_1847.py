# Generated by Django 3.1.7 on 2021-04-24 18:47

from django.db import migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('yama', '0005_auto_20210424_1134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pitimage',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to='static/uploads', verbose_name='Изображение'),
        ),
    ]
