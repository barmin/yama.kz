import json

from django.core.paginator import Paginator
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from yama.forms import PitForm
from yama.models import Pit, PitStatus, PitImage
from yama.services import pit_maps, pit_pipeline
from yama.services.pit_pipeline import get_filtered_pits


def index(request) -> HttpResponse:
    print(request.GET.getlist('markers'))
    pits = get_filtered_pits(request)
    return render(request, 'yama/index.html', {
        'pits': pits,
        'map_pits': json.dumps(pit_maps.get_markers_for_map(pits))
    })


def pit_list(request) -> HttpResponse:
    pits = get_filtered_pits(request)
    paginator = Paginator(pits, 12)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'yama/index.html', {'pits': pits,
                                               'page_obj': page_obj,
                                               'is_list': True})


def pit_detail(request, pit_id) -> HttpResponse:
    pit = Pit.objects.filter(pk=pit_id).first()
    if not pit_pipeline.pit_has_displayed(pit, request.user):
        return render(request, 'yama/404.html', status=404)
    context = {
        'pit': pit,
        'map_pits': json.dumps(pit_maps.get_markers_for_map([pit]))
    }
    return render(request, 'yama/pit-detail.html', context)


def pit_add(request) -> HttpResponse:
    if not request.user.is_authenticated:
        return redirect('index')
    pit_form = PitForm()
    if request.method == 'POST':
        pit_form = PitForm(request.POST)
        if pit_form.is_valid():
            result = pit_form.save()
            result.author = request.user
            result.save()
            for image in request.FILES.getlist('pit_image[]')[:5]:
                PitImage.objects.create(pit=result, image=image)
            return JsonResponse({
                'success': True,
                'pit_id': result.id
            })
        return JsonResponse({
            'success': False,
            'error': 'Проверьте правильность введенных данных, и укажите точку на карте'
        })
    return render(request, 'yama/pit-add.html', {'pit_form': pit_form})


def map_pit_list(request) -> HttpResponse:
    pits = get_filtered_pits(request)
    return JsonResponse(pit_maps.get_markers_for_map(pits))


def page404(request, exception) -> HttpResponse:
    return render(request, 'yama/404.html')
