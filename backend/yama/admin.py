from django.conf.urls import url
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.html import format_html

from yama.models import PitImage, Pit, PitStatus
from yama.services import database


class PitImageInline(admin.TabularInline):
    model = PitImage


class PitStatusInline(admin.TabularInline):
    model = PitStatus


def set_status(request, pit_id, status):
    if status == 'deleted':
        database.set_deleted_status(pit_id)
    elif status == 'on_police':
        database.set_on_police_status(pit_id)
    elif status == 'moderate_discard':
        database.set_moderate_discard_status(pit_id)
    return redirect('/admin/yama/pit/')


class PitAdmin(admin.ModelAdmin):
    list_display = ['latitude', 'longitude', 'pit_type', 'author', 'created',
                    'pit_actions', 'pit_status']
    # list_filter = (
    #     ('category', admin.RelatedOnlyFieldListFilter),
    # )
    inlines = [PitImageInline, PitStatusInline]

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<pit_id>.+)/status/(?P<status>.+)/$',
                set_status,
                name='set_status',
            ),
        ]
        return custom_urls + urls

    def pit_status(self, obj):
        return obj.status

    def pit_actions(self, obj):
        if obj.status:
            if obj.status.status == PitStatus.MODERATE:
                return format_html(
                    '<a class="button" href="{}">Отклонить</a>&nbsp;'
                    '<a class="button" href="{}">Подтвердить</a>&nbsp;'
                    '<a class="button" href="{}">Удалить*</a>',
                    reverse('admin:set_status', args=[obj.pk, 'moderate_discard']),
                    reverse('admin:set_status', args=[obj.pk, 'on_police']),
                    reverse('admin:set_status', args=[obj.pk, 'deleted']),
                )

    pit_actions.short_description = 'Действия ямы'
    pit_actions.allow_tags = True

    # exclude = ['slug']


admin.site.register(Pit, PitAdmin)
