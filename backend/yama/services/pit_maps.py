from django.template import loader

from yama.models import Pit, PitStatus

STATUSES = {
    PitStatus.MODERATE: '/static/images/marker-moderate.png',
    PitStatus.ON_POLICE: '/static/images/marker-onpolice.png',
    PitStatus.POLICE_NO_ANSWER: '/static/images/marker-noanswer.png',
    PitStatus.POLICE_DISCARD: '/static/images/marker-discard.png',
    PitStatus.ON_PLANE: '/static/images/marker-onplane.png',
    PitStatus.DONE: '/static/images/marker-fixed.png'
}


def get_marker_card(pit: Pit) -> str:
    return loader.render_to_string('yama/ui/pit-item.html', {'pit': pit, 'is_card': True})


def get_markers_for_map(pit_list) -> dict:
    features = []
    for pit in pit_list:
        pit_object = {
            "type": "Feature",
            "id": pit.id,
            "geometry": {
                "type": "Point",
                "coordinates": [float(pit.latitude), float(pit.longitude)]
            },
            "properties": {
                "balloonContentBody": get_marker_card(pit),
                "hintContent": str(pit.status),
                "clusterCaption": pit.address
            },
            "options": {
                "iconLayout": "default#image",
                "iconImageHref": STATUSES[pit.status.status],
                "iconImageSize": [50, 54],
                "iconImageOffset": [-25, -44],
                "openEmptyBalloon": True
            }
        }
        features.append(pit_object)
    response = {
        "type": "FeatureCollection",
        "features": features
    }
    return response
