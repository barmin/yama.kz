from typing import Union

from django.contrib.auth import get_user_model

from yama.models import Pit, PitStatus


def send_to_police(pit: Union[Pit, int]) -> bool:
    # TODO: релизнуть в дальнейшем
    return True


def pit_has_displayed(pit: Union[Pit, None], user: get_user_model()):
    if not pit:
        return False
    if pit.status.status in [PitStatus.MODERATE, PitStatus.MODERATE_DISCARD]:
        return pit.author == user
    if pit.status.status == PitStatus.DELETED:
        return False
    return True


def get_filtered_pits(request):
    pits = Pit.objects.all()

    pits_ids = []
    for pit in pits:
        if pit_has_displayed(pit, request.user):
            pits_ids.append(pit.id)
    pits = pits.filter(id__in=pits_ids)

    region = request.GET.get('region')
    if region:
        pits = pits.filter(region=region)
    only_my = request.GET.get('onlyMy')
    if only_my:
        pits = pits.filter(author=request.user)
    city = request.GET.get('city')
    if city:
        pits = pits.filter(city=city)
    pit_type = request.GET.get('pit_type')
    if pit_type:
        pits = pits.filter(pit_type=pit_type)
    status = request.GET.getlist('markers')
    if status:
        pits_ids = []
        for pit in pits:
            if pit.status.status in status:
                pits_ids.append(pit.id)
        pits = pits.filter(id__in=pits_ids)
    with_photo = request.GET.get('photo')
    if with_photo:
        pits_ids = []
        for pit in pits:
            if len(pit.images.all()) > 0:
                pits_ids.append(pit.id)
        pits = pits.filter(id__in=pits_ids)
    return pits
