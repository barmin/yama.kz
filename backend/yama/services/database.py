from typing import Union

from yama.models import Pit, PitStatus


# PIT Statuses
from yama.services import pit_pipeline


def _set_status(pit: Union[Pit, int], status: str) -> None:
    if type(pit) is Pit:
        PitStatus.objects.create(pit=pit, status=status)
    else:
        PitStatus.objects.create(pit_id=pit, status=status)


def set_moderate_status(pit: Union[Pit, int]) -> None:
    _set_status(pit, PitStatus.MODERATE)


def set_deleted_status(pit: Union[Pit, int]) -> None:
    _set_status(pit, PitStatus.DELETED)


def set_on_police_status(pit: Union[Pit, int]) -> None:
    if pit_pipeline.send_to_police(pit):
        _set_status(pit, PitStatus.ON_POLICE)

def set_on_plane_status(pit: Union[Pit, int]) -> None:
    _set_status(pit, PitStatus.ON_PLANE)


def set_police_no_answer_status(pit: Union[Pit, int]) -> None:
    _set_status(pit, PitStatus.POLICE_NO_ANSWER)


def set_police_discard_status(pit: Union[Pit, int]) -> None:
    _set_status(pit, PitStatus.POLICE_DISCARD)


def set_moderate_discard_status(pit: Union[Pit, int]) -> None:
    _set_status(pit, PitStatus.MODERATE_DISCARD)


def set_done_status(pit: Union[Pit, int]) -> None:
    _set_status(pit, PitStatus.DONE)
