from django.contrib.auth import get_user_model
from django.db import models
from sorl.thumbnail import ImageField
User = get_user_model()


class Pit(models.Model):
    PIT_TYPE_CHOICES = (
        ('on_road', 'Яма на дороге'),
        ('on_yard', 'Яма во дворе')
    )

    latitude = models.CharField('Широта', max_length=30)
    longitude = models.CharField('Долгота', max_length=30)
    place_description = models.CharField('Описание расположения',
                                         max_length=100)
    city = models.CharField('Город', max_length=50)
    region = models.CharField('Регион', max_length=50)
    address = models.CharField('Полный адрес', max_length=300)

    pit_type = models.CharField('Тип повреждения', max_length=10,
                                choices=PIT_TYPE_CHOICES)
    pit_description = models.CharField('Описание повреждения', max_length=200)

    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                               blank=True)

    created = models.DateTimeField('Дата создания', auto_now=True,
                                   editable=False)
    edited = models.DateTimeField('Дата изменения', auto_now_add=True,
                                  editable=False)

    @property
    def status(self):
        return self.statuses.last()


class PitImage(models.Model):
    pit = models.ForeignKey(Pit, default=None, related_name='images',
                            on_delete=models.CASCADE)
    image = ImageField('Изображение', upload_to='static/uploads')


class PitStatus(models.Model):
    MODERATE = 'moderate'
    ON_POLICE = 'on_police'
    POLICE_DISCARD = 'police_discard'
    POLICE_NO_ANSWER = 'police_no_answer'
    ON_PLANE = 'on_plane'
    DONE = 'done'
    MODERATE_DISCARD = 'moderate_discard'
    DELETED = 'deleted'

    PIT_STATUS_CHOICES = (
        (MODERATE, 'На модерации'),
        (ON_POLICE, 'В полиции'),
        (POLICE_NO_ANSWER, 'Нет ответа от полиции'),
        (POLICE_DISCARD, 'Отказано полицией'),
        (ON_PLANE, 'Ремонт запланирован'),
        (MODERATE_DISCARD, 'Отказано модератором'),
        (DONE, 'Отремонтировано'),
        (DELETED, 'Удалено'),
    )

    pit = models.ForeignKey(Pit, default=None, related_name='statuses',
                            on_delete=models.CASCADE)
    status = models.CharField('Статус заявки', max_length=20,
                              choices=PIT_STATUS_CHOICES)
    created = models.DateTimeField('Дата создания', auto_now=True)

    def __str__(self):
        return dict(self.PIT_STATUS_CHOICES)[self.status]
