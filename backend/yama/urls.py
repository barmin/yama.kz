"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

import yama.views as views

urlpatterns = [
    path('', views.pit_list, name="pit_list"),
    path('maps/list/', views.map_pit_list, name="pit_map_list"),
    path('add/', views.pit_add, name="pit_add"),
    path('<pit_id>/', views.pit_detail, name="pit_detail"),
]
