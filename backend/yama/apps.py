from django.apps import AppConfig


class YamaConfig(AppConfig):
    name = 'yama'

    def ready(self):
        import yama.signals
