from django.db.models import signals

from django.dispatch import receiver

from yama.models import Pit
from yama.services import database


@receiver(signals.post_save, sender=Pit)
def create_pit(sender: Pit, instance: Pit, created, **kwargs):
    if created:
        database.set_moderate_status(instance)
