from django.forms import ModelForm
from django import forms

from yama.models import Pit


class PitForm(ModelForm):
    # latitude = forms
    # author = forms.CharField(widget=forms.HiddenInput())
    latitude = forms.CharField(widget=forms.HiddenInput())
    longitude = forms.CharField(widget=forms.HiddenInput())
    city = forms.CharField(widget=forms.HiddenInput())
    region = forms.CharField(widget=forms.HiddenInput())
    address = forms.CharField(widget=forms.HiddenInput())
    class Meta:
        model = Pit
        fields = ('latitude', 'longitude', 'place_description', 'city',
                  'region', 'address', 'pit_type', 'pit_description')
